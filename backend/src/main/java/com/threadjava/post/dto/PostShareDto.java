package com.threadjava.post.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class PostShareDto {
    private String email;
    private UUID postId;
    private UUID userId;
}
