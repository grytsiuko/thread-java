package com.threadjava.post;

import com.threadjava.post.dto.PostDetailsQueryResult;
import com.threadjava.post.model.Post;
import com.threadjava.post.dto.PostListQueryResult;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PostsRepository extends JpaRepository<Post, UUID> {

    @Query("SELECT new com.threadjava.post.dto.PostListQueryResult(p.id, p.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COUNT(*) FROM p.comments pc WHERE pc.deleted is null OR pc.deleted = False ), " +
            "p.createdAt, i, p.user) " +
            "FROM Post p " +
            "LEFT JOIN p.image i " +
            "WHERE ( p.deleted is null OR p.deleted = False) " +
            "       AND " +
            "      ( cast(:showUserId as string) is null OR p.user.id = :showUserId ) " +
            "       AND " +
            "      ( cast(:hideUserId as string) is null OR p.user.id <> :hideUserId ) " +
            "       AND " +
            "      ( cast(:likedByUserId as string) is null OR " +
            "         EXISTS (SELECT pr " +
            "                 FROM PostReaction pr " +
            "                 WHERE pr.user.id = :likedByUserId AND " +
            "                       pr.post.id = p.id AND " +
            "                       pr.isLike = true) ) " +
            "order by p.createdAt desc" )
    List<PostListQueryResult> findAllPosts(@Param("showUserId") UUID showUserId,
                                           @Param("hideUserId") UUID hideUserId,
                                           @Param("likedByUserId") UUID likedByUserId,
                                           Pageable pageable);

    @Query("SELECT new com.threadjava.post.dto.PostDetailsQueryResult(p.id, p.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COUNT(*) FROM p.comments pc WHERE pc.deleted is null OR pc.deleted = False ), " +
            "p.createdAt, p.updatedAt, i, p.user) " +
            "FROM Post p " +
            "LEFT JOIN p.image i " +
            "WHERE p.id = :id")
    Optional<PostDetailsQueryResult> findPostById(@Param("id") UUID id);

    @Transactional
    @Modifying
    @Query("UPDATE Post p " +
            "SET p.deleted = true " +
            "WHERE p.id = :id")
    void softDelete(@Param("id") UUID id);
}
