package com.threadjava.post;

import com.threadjava.comment.CommentRepository;
import com.threadjava.post.dto.*;
import com.threadjava.post.model.Post;
import com.threadjava.postReactions.PostReactionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PostsService {
    @Autowired
    private PostsRepository postsCrudRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private PostReactionsRepository postReactionsRepository;

    public List<PostListDto> getAllPosts(Integer from, Integer count,
                                         UUID showUserId, UUID hideUserId, UUID likedByUserId) {
        var pageable = PageRequest.of(from / count, count);
        return postsCrudRepository
                .findAllPosts(showUserId, hideUserId, likedByUserId, pageable)
                .stream()
                .map(PostMapper.MAPPER::postListToPostListDto)
                .collect(Collectors.toList());
    }

    public PostDetailsDto getPostById(UUID id) {
        var post = postsCrudRepository.findPostById(id)
                .map(PostMapper.MAPPER::postToPostDetailsDto)
                .orElseThrow();

        var comments = commentRepository.findAllByPostId(id)
                .stream()
                .map(PostMapper.MAPPER::commentToCommentDto)
                .collect(Collectors.toList());
        post.setComments(comments);

        var reactions = postReactionsRepository.findAllByPostId(id)
                .stream()
                .map(PostMapper.MAPPER::postReactionToPostReactionDto)
                .collect(Collectors.toList());
        post.setReactions(reactions);

        return post;
    }

    public PostCreationResponseDto create(PostCreationDto postDto) {
        Post post = PostMapper.MAPPER.postDetailsDtoToPost(postDto);
        Post postCreated = postsCrudRepository.save(post);
        return PostMapper.MAPPER.postToPostCreationResponseDto(postCreated);
    }

    public void update(PostUpdateDto postDto) {
        Post post = postsCrudRepository
                .findById(postDto.getPostId())
                .orElseThrow();
        post.setBody(postDto.getBody());
        postsCrudRepository.save(post);
    }

    public void delete(UUID id) {
        postsCrudRepository.softDelete(id);
    }
}
