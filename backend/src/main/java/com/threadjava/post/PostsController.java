package com.threadjava.post;


import com.threadjava.mail.Mail;
import com.threadjava.post.dto.*;
import com.threadjava.post.model.Post;
import com.threadjava.users.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/posts")
public class PostsController {
    @Value(value = "${mail.email}")
    private String EMAIL;
    @Value(value = "${mail.password}")
    private String PASSWORD;
    @Value(value = "${mail.host}")
    private String HOST;
    @Value(value = "${mail.port}")
    private String PORT;
    @Autowired
    private UsersService usersService;
    @Autowired
    private PostsService postsService;
    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping
    public List<PostListDto> get(@RequestParam(defaultValue = "0") Integer from,
                                 @RequestParam(defaultValue = "10") Integer count,
                                 @RequestParam(required = false) UUID showUserId,
                                 @RequestParam(required = false) UUID hideUserId,
                                 @RequestParam(required = false) UUID likedByUserId) {
        return postsService.getAllPosts(from, count, showUserId, hideUserId, likedByUserId);
    }

    @GetMapping("/{id}")
    public PostDetailsDto get(@PathVariable UUID id) {
        return postsService.getPostById(id);
    }

    @PostMapping
    public PostCreationResponseDto post(@RequestBody PostCreationDto postDto) {
        postDto.setUserId(getUserId());
        var item = postsService.create(postDto);
        template.convertAndSend("/topic/new_post", item);
        return item;
    }

    @PutMapping
    public ResponseEntity<Long> put(@RequestBody PostUpdateDto postDto) {
        var originalPost = postsService.getPostById(postDto.getPostId());

        if (!originalPost.getUser().getId().equals(getUserId())) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        postDto.setUserId(getUserId());
        postsService.update(postDto);
        template.convertAndSend("/topic/post/updated", "Post was updated!");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> delete(@PathVariable UUID id) {
        var originalPost = postsService.getPostById(id);

        if (!originalPost.getUser().getId().equals(getUserId())) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        postsService.delete(id);
        template.convertAndSend("/topic/post/deleted", "Post was deleted!");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/share")
    public ResponseEntity<Long> share(@RequestBody PostShareDto postDto) {
        postDto.setUserId(getUserId());

        String email = postDto.getEmail();
        var post = postsService.getPostById(postDto.getPostId());
        var shareUser = usersService.getUserById(postDto.getUserId());

        new Mail(EMAIL, PASSWORD, HOST, PORT).sendSharedPost(email, shareUser, post);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
