package com.threadjava.mail;

import com.threadjava.auth.model.AuthUser;
import com.threadjava.post.dto.PostDetailsDto;
import com.threadjava.users.dto.UserDetailsDto;
import lombok.AllArgsConstructor;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;
import java.util.UUID;
import javax.mail.internet.*;

@AllArgsConstructor
public class Mail {
    private final String email;
    private final String password;
    private final String host;
    private final String port;

    public void sendLikedPost(UserDetailsDto postUser,
                              UserDetailsDto likeUser,
                              PostDetailsDto postDetails) {
        String toEmail = postUser.getEmail();
        String header = "Thread";
        String body = "Hello!<br><br>" +
                "Your post was liked by <i>" + likeUser.getUsername() + "</i><br><br>" +
                "Post: http://localhost:3000/share/" + postDetails.getId() + "<br>";
        send(toEmail, header, body);
    }

    public void sendSharedPost(String toEmail,
                               UserDetailsDto shareUser,
                               PostDetailsDto postDetails) {
        String header = "Thread";
        String body = "Hello!<br><br>" +
                "<i>" + shareUser.getUsername() + "</i> shared a post with you<br><br>" +
                "Post: http://localhost:3000/share/" + postDetails.getId() + "<br>";
        send(toEmail, header, body);
    }

    public void sendRequestPassword(String toEmail, UUID userId) {
        String header = "Thread";
        String body = "Hello!<br><br>" +
                "Here is a link to reset your password:<br>" +
                "http://localhost:3000/password/" + userId + "<br>";
        send(toEmail, header, body);
    }

    private void send(String toEmail, String header, String body) {
        System.out.println("Sending email to " + toEmail);
        Properties prop = new Properties();
        prop.put("mail.smtp.auth", true);
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.host", host);
        prop.put("mail.smtp.port", port);

        Session session = Session.getInstance(prop, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(email, password);
            }
        });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(email));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(toEmail)
            );
            message.setSubject(header);

            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(body, "text/html");

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(mimeBodyPart);

            message.setContent(multipart);

            Transport.send(message);
            System.out.println("Sent email to " + toEmail);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Errors while sending email to " + toEmail);
        }
    }
}
