package com.threadjava.commentReactions.dto;

import com.threadjava.users.dto.UserShortDto;
import lombok.Data;
import java.util.UUID;

@Data
public class CommentReactionDto {
    private UUID id;
    private Boolean isLike;
    private UserShortDto user;
}
