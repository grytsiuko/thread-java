package com.threadjava.comment.dto;

import com.threadjava.commentReactions.dto.CommentReactionDto;
import com.threadjava.users.dto.UserShortDto;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
public class CommentDetailsDto {
    private UUID id;
    private String body;
    private UserShortDto user;
    private long likeCount;
    private long dislikeCount;
    private Date createdAt;
    private Date updatedAt;
    private UUID postId;
    private List<CommentReactionDto> reactions = new ArrayList<>();
}
