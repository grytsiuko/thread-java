package com.threadjava.comment.dto;

import com.threadjava.image.model.Image;
import com.threadjava.users.dto.UserShortDto;
import com.threadjava.users.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentDetailsQueryResult {
    private UUID id;
    private String body;
    private long likeCount;
    private long dislikeCount;
    private Date createdAt;
    private Date updatedAt;
    private User user;
    private UUID postId;
}
