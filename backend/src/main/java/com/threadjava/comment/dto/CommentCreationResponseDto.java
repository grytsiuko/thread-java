package com.threadjava.comment.dto;

import com.threadjava.commentReactions.dto.CommentReactionDto;
import com.threadjava.users.dto.UserShortDto;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class CommentCreationResponseDto {
    private UUID id;
    private String body;
    private UserShortDto user;
    private UUID postId;
    private List<CommentReactionDto> reactions = new ArrayList<>();
}
