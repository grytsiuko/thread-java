package com.threadjava.comment;

import com.threadjava.comment.dto.CommentCreationResponseDto;
import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentDetailsQueryResult;
import com.threadjava.comment.dto.CommentSaveDto;
import com.threadjava.comment.model.Comment;
import com.threadjava.commentReactions.dto.CommentReactionDto;
import com.threadjava.commentReactions.model.CommentReaction;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CommentMapper {
    CommentMapper MAPPER = Mappers.getMapper(CommentMapper.class);

    @Mapping(source = "post.id", target = "postId")
    @Mapping(source = "user.avatar", target = "user.image")
    CommentCreationResponseDto commentToCommentCreationResponseDto(Comment comment);

    @Mapping(source = "user.avatar", target = "user.image")
    @Mapping(target = "reactions", ignore = true)
    CommentDetailsDto commentToCommentDetailsDto(CommentDetailsQueryResult comment);

    @Mapping(source = "postId", target = "post.id")
    @Mapping(source = "userId", target = "user.id")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "reactions", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "deleted", constant = "false")
    Comment commentSaveDtoToModel(CommentSaveDto commentDto);

    @Mapping(source = "user.avatar", target = "user.image")
    public abstract CommentReactionDto commentReactionToCommentReactionDto(CommentReaction commentReaction);
}
