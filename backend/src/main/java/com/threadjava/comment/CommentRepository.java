package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsQueryResult;
import com.threadjava.comment.model.Comment;
import com.threadjava.post.dto.PostDetailsQueryResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CommentRepository extends JpaRepository<Comment, UUID> {

    @Query("SELECT new com.threadjava.comment.dto.CommentDetailsQueryResult(c.id, c.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "c.createdAt, c.updatedAt, c.user, c.post.id) " +
            "FROM Comment c " +
            "WHERE c.post.id = :id " +
            "      AND " +
            "      (c.deleted is null OR c.deleted = false )")
    List<CommentDetailsQueryResult> findAllByPostId(@Param("id") UUID postId);

    @Query("SELECT new com.threadjava.comment.dto.CommentDetailsQueryResult(c.id, c.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "c.createdAt, c.updatedAt, c.user, c.post.id) " +
            "FROM Comment c " +
            "WHERE c.id = :id")
    Optional<CommentDetailsQueryResult> findCommentById(@Param("id") UUID commentId);

    @Transactional
    @Modifying
    @Query("UPDATE Comment c " +
            "SET c.deleted = true " +
            "WHERE c.id = :id")
    void softDelete(@Param("id") UUID id);
}