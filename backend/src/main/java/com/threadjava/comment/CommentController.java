package com.threadjava.comment;

import com.threadjava.comment.dto.CommentCreationResponseDto;
import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentSaveDto;
import com.threadjava.comment.dto.CommentUpdateDto;
import com.threadjava.post.dto.PostUpdateDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import java.util.UUID;
import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/comments")
public class CommentController {
    @Autowired
    private CommentService commentService;
    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping("/{id}")
    public CommentDetailsDto get(@PathVariable UUID id) {
        return commentService.getCommentById(id);
    }

    @PostMapping
    public CommentCreationResponseDto post(@RequestBody CommentSaveDto commentDto) {
        commentDto.setUserId(getUserId());
        var commentResponse = commentService.create(commentDto);
        template.convertAndSend("/topic/post/comment", "New comment!");
        return commentResponse;
    }

    @PutMapping
    public ResponseEntity<Long> put(@RequestBody CommentUpdateDto commentDto) {
        var originalComment = commentService.getCommentById(commentDto.getCommentId());

        if(!originalComment.getUser().getId().equals(getUserId())){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        commentDto.setUserId(getUserId());
        commentService.update(commentDto);
        template.convertAndSend("/topic/comment/updated", "Comment was updated!");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> delete(@PathVariable UUID id) {
        var originalComment = commentService.getCommentById(id);

        if(!originalComment.getUser().getId().equals(getUserId())){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        commentService.delete(id);
        template.convertAndSend("/topic/comment/deleted", "Comment was deleted!");
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
