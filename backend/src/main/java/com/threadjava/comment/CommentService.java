package com.threadjava.comment;

import com.threadjava.comment.dto.CommentCreationResponseDto;
import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentSaveDto;
import com.threadjava.comment.dto.CommentUpdateDto;
import com.threadjava.comment.model.Comment;
import com.threadjava.commentReactions.CommentReactionsRepository;
import com.threadjava.post.PostsRepository;
import com.threadjava.post.dto.PostUpdateDto;
import com.threadjava.post.model.Post;
import com.threadjava.users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private CommentReactionsRepository commentReactionsRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private PostsRepository postsRepository;

    public CommentDetailsDto getCommentById(UUID id) {
        var comment = commentRepository.findCommentById(id)
                .map(CommentMapper.MAPPER::commentToCommentDetailsDto)
                .orElseThrow();

        var reactions = commentReactionsRepository.findAllByCommentId(id)
                .stream()
                .map(CommentMapper.MAPPER::commentReactionToCommentReactionDto)
                .collect(Collectors.toList());
        comment.setReactions(reactions);

        return comment;
    }

    public CommentCreationResponseDto create(CommentSaveDto commentDto) {
        var comment = CommentMapper.MAPPER.commentSaveDtoToModel(commentDto);
        var postCreated = commentRepository.save(comment);
        return CommentMapper.MAPPER.commentToCommentCreationResponseDto(postCreated);
    }

    public void update(CommentUpdateDto commentDto) {
        Comment comment = commentRepository
                .findById(commentDto.getCommentId())
                .orElseThrow();
        comment.setBody(commentDto.getBody());
        commentRepository.save(comment);
    }

    public void delete(UUID id) {
        commentRepository.softDelete(id);
    }
}
