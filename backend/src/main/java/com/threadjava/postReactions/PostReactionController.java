package com.threadjava.postReactions;

import com.threadjava.mail.Mail;
import com.threadjava.post.PostsService;
import com.threadjava.postReactions.dto.ReceivedPostReactionDto;
import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import com.threadjava.users.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/postreaction")
public class PostReactionController {
    @Value(value = "${mail.email}")
    private String EMAIL;
    @Value(value = "${mail.password}")
    private String PASSWORD;
    @Value(value = "${mail.host}")
    private String HOST;
    @Value(value = "${mail.port}")
    private String PORT;
    @Value(value = "${mail.notifyLikePost}")
    private String NOTIFY_LIKE_POST;
    @Autowired
    private PostsService postsService;
    @Autowired
    private UsersService usersService;
    @Autowired
    private PostReactionService postReactionService;
    @Autowired
    private SimpMessagingTemplate template;

    @PutMapping
    public Optional<ResponsePostReactionDto> setReaction(@RequestBody ReceivedPostReactionDto postReaction) {
        postReaction.setUserId(getUserId());
        var reaction = postReactionService.setReaction(postReaction);

        if (reaction.isPresent()) {

            // set in properties, if to notify about post like
            // gmail provides only 100 emails per day,
            // so it is convenient to be able to turn off it
            if (Boolean.parseBoolean(NOTIFY_LIKE_POST) && reaction.get().getIsLike()) {
                var post = postsService.getPostById(reaction.get().getPostId());
                var postUser = usersService.getUserById(post.getUser().getId());
                var likeUser = usersService.getUserById(reaction.get().getUserId());

                if (!likeUser.getId().equals(postUser.getId())) {
                    // to immediately display like change on the user side, even
                    // if some problems with smtp happen
                    new Thread(() -> new Mail(EMAIL, PASSWORD, HOST, PORT).sendLikedPost(postUser, likeUser, post)).start();
                }
            }

            if (reaction.get().getIsLike()) {
                template.convertAndSend("/topic/like", "Post was liked!");
            } else {
                template.convertAndSend("/topic/dislike", "Post was disliked!");
            }
        }

        return reaction;
    }
}
