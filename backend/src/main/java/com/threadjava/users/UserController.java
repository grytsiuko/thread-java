package com.threadjava.users;

import com.threadjava.users.dto.UpdateUserDto;
import com.threadjava.users.dto.UserAvatarDto;
import com.threadjava.users.dto.UserDetailsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UsersService userDetailsService;

    @GetMapping
    public UserDetailsDto getUser() {
        return userDetailsService.getUserById(getUserId());
    }

    @PutMapping
    public ResponseEntity<Long> put(@RequestBody UpdateUserDto userDto){
        userDto.setId(getUserId());
        userDetailsService.update(userDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/photo")
    public ResponseEntity<Long> putPhoto(@RequestBody UserAvatarDto avatarDto){
        avatarDto.setId(getUserId());
        userDetailsService.updateAvatar(avatarDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
