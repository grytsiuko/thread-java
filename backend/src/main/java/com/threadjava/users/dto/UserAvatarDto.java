package com.threadjava.users.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class UserAvatarDto {
    private UUID id;
    private UUID imageId;
}
