package com.threadjava.users.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class UpdateUserDto {
    private UUID id;
    private String status;
    private String username;
    private String email;
}
