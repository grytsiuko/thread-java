package com.threadjava.users;

import com.threadjava.auth.model.AuthUser;
import com.threadjava.image.ImageRepository;
import com.threadjava.users.dto.UpdateUserDto;
import com.threadjava.users.dto.UserAvatarDto;
import com.threadjava.users.dto.UserDetailsDto;
import com.threadjava.users.dto.UserShortDto;
import com.threadjava.users.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;
@Service
public class UsersService implements UserDetailsService {
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private ImageRepository imageRepository;

    @Override
    public AuthUser loadUserByUsername(String email) throws UsernameNotFoundException {
        return usersRepository
                .findByEmail(email)
                .map(user -> new AuthUser(user.getId(), user.getEmail(), user.getPassword()))
                .orElseThrow(() -> new UsernameNotFoundException(email));
    }

    public Optional<User> get(UUID id) {
        return usersRepository.findById(id);
    }

    public UserDetailsDto getUserById(UUID id) {
        return usersRepository
                .findById(id)
                .map(user -> UserMapper.MAPPER.userToUserDetailsDto(user))
                .orElseThrow(() -> new UsernameNotFoundException("No user found with username"));
    }

    public void update(UpdateUserDto userDto) {
        User user = usersRepository
                .findById(userDto.getId())
                .orElseThrow();
        user.setStatus(userDto.getStatus());
        user.setUsername(userDto.getUsername());
        user.setEmail(userDto.getEmail());
        usersRepository.save(user);
    }

    public void updateAvatar(UserAvatarDto avatarDto) {
        User user = usersRepository
                .findById(avatarDto.getId())
                .orElseThrow();

        var image = imageRepository
                .findById(avatarDto.getImageId())
                .orElseThrow();

        user.setAvatar(image);
        usersRepository.save(user);
    }

    public void save(User user) {
        usersRepository.save(user);
    }
}
