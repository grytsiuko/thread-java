package com.threadjava.auth.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResetPasswordDTO {
    private UUID userId;
    private String password;
}
