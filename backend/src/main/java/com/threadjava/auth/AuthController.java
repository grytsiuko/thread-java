package com.threadjava.auth;

import com.threadjava.auth.dto.*;
import com.threadjava.mail.Mail;
import com.threadjava.users.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    private AuthService authService;
    @Autowired
    private UsersService userDetailsService;
    @Value(value = "${mail.email}")
    private String EMAIL;
    @Value(value = "${mail.password}")
    private String PASSWORD;
    @Value(value = "${mail.host}")
    private String HOST;
    @Value(value = "${mail.port}")
    private String PORT;

    @PostMapping("/register")
    public AuthUserDTO signUp(@RequestBody UserRegisterDto user) throws Exception {
        return authService.register(user);
    }

    @PostMapping("/login")
    public AuthUserDTO login(@RequestBody UserLoginDTO user) throws Exception {
        return authService.login(user);
    }

    @PostMapping("/request")
    public ResponseEntity<Long> request(@RequestBody UserRequestPasswordDto requestPasswordDto) {
        String email = requestPasswordDto.getEmail();
        var user = userDetailsService.loadUserByUsername(email);
        UUID userId = user.getId();
        new Mail(EMAIL, PASSWORD, HOST, PORT).sendRequestPassword(email, userId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/reset")
    public ResponseEntity<Long> reset(@RequestBody UserResetPasswordDTO resetPasswordDTO) {
        authService.reset(resetPasswordDTO);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
