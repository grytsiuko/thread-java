import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Popup, Placeholder, Container, Image } from 'semantic-ui-react';
import { getUserImgLink } from '../../helpers/imageHelper';

const ReactionsList = ({ getWrapper, wrapperId, getReactions, trigger }) => {
  const [reactionsList, setReactionsList] = useState(null);
  const blockWidth = 200;

  return (
    <Popup
      on="hover"
      onOpen={() => {
        setReactionsList(null);
        getWrapper(wrapperId).then(r => setReactionsList(getReactions(r)));
      }}
      trigger={trigger}
      popperDependecies={[!!reactionsList]}
      position="left center"
      hoverable
      style={{ width: blockWidth, zIndex: 9999 }}
    >
      {reactionsList === null ? (
        <Placeholder style={{ width: blockWidth }}>
          <Placeholder.Paragraph>
            <Placeholder.Line length="medium" />
            <Placeholder.Line length="short" />
            <Placeholder.Line length="short" />
          </Placeholder.Paragraph>
        </Placeholder>
      ) : (
        <Container style={{ width: blockWidth }}>
          {
            reactionsList.length > 0
              ? reactionsList.map(reaction => (
                <p>
                  <Image avatar src={getUserImgLink(reaction.user.image)} style={{ marginRight: 10 }} />
                  {reaction.user.username}
                </p>
              ))
              : 'No one yet'
          }
        </Container>
      )}
    </Popup>
  );
};

ReactionsList.propTypes = {
  getWrapper: PropTypes.func.isRequired,
  wrapperId: PropTypes.objectOf(PropTypes.any).isRequired,
  getReactions: PropTypes.func.isRequired,
  trigger: PropTypes.objectOf(PropTypes.any).isRequired
};

export default ReactionsList;
