import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Stomp } from '@stomp/stompjs';
import SockJS from 'sockjs-client';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

const Notifications = ({ user, applyPost }) => {
  const [stompClient] = useState(Stomp.over(new SockJS('/ws')));

  useEffect(() => {
    if (!user) {
      return undefined;
    }

    stompClient.debug = () => { };
    stompClient.connect({}, () => {
      console.log('connected');

      const { id } = user;
      const infoMessage = message => {
        NotificationManager.info(message.body);
      };

      stompClient.subscribe('/topic/comment/deleted', infoMessage);

      stompClient.subscribe('/topic/comment/updated', infoMessage);

      stompClient.subscribe('/topic/post/deleted', infoMessage);

      stompClient.subscribe('/topic/post/updated', infoMessage);

      stompClient.subscribe('/topic/post/comment', infoMessage);

      stompClient.subscribe('/topic/like', infoMessage);

      stompClient.subscribe('/topic/dislike', infoMessage);

      stompClient.subscribe('/topic/commentLike', infoMessage);

      stompClient.subscribe('/topic/commentDislike', infoMessage);

      stompClient.subscribe('/topic/new_post', message => {
        NotificationManager.info('New post!');
        const post = JSON.parse(message.body);
        if (post.userId !== id) {
          applyPost(post.id);
        }
      });
    });

    return () => {
      stompClient.disconnect();
    };
  });

  return <NotificationContainer />;
};

Notifications.defaultProps = {
  user: undefined
};

Notifications.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  applyPost: PropTypes.func.isRequired
};

export default Notifications;
