import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Segment, Message } from 'semantic-ui-react';

const PasswordForm = ({ userId, resetPassword: reset }) => {
  const [password, setPassword] = useState(null);
  const [isLoading, setLoading] = useState(false);
  const [isSuccess, setSuccess] = useState(false);
  const [isError, setError] = useState(false);
  const [isPasswordValid, setPasswordValid] = useState(false);

  const passwordChanged = value => {
    setPassword(value === '' ? null : value);
    setPasswordValid(value !== '');
  };

  const handleReset = async () => {
    setError(false);
    setSuccess(false);
    if (!isPasswordValid || isLoading) {
      return;
    }
    setLoading(true);
    reset({ userId, password })
      .then(() => setSuccess(true))
      .catch(() => setError(true))
      .finally(() => setLoading(false));
  };

  return (
    <Form name="registrationForm" size="large" onSubmit={handleReset}>
      <Segment>
        {isError && (
          <Message negative>
            <Message.Header>
              Error occurred
            </Message.Header>
          </Message>
        )}
        {isSuccess && (
          <Message positive>
            <Message.Header>
              Password reset successfully
            </Message.Header>
          </Message>
        )}
        <Form.Input
          fluid
          icon="lock"
          iconPosition="left"
          placeholder="Password"
          type="password"
          onChange={ev => passwordChanged(ev.target.value)}
          error={!isPasswordValid}
          onBlur={() => setPasswordValid(Boolean(password))}
        />
        <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
          Reset
        </Button>
      </Segment>
    </Form>
  );
};

PasswordForm.propTypes = {
  resetPassword: PropTypes.func.isRequired,
  userId: PropTypes.objectOf(PropTypes.any).isRequired
};

export default PasswordForm;
