import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { Modal, Input, Icon } from 'semantic-ui-react';
import validator from 'validator';

import styles from './styles.module.scss';
import { sharePostViaEmail } from '../../services/postService';

const SharedPostLink = ({ postId, close }) => {
  const [isLoading, setLoading] = useState(false);
  const [email, setEmail] = useState('');
  const [isEmailValid, setEmailValid] = useState(false);
  const [sent, setSent] = useState(false);
  const [copied, setCopied] = useState(false);
  let input = useRef();

  const copyToClipboard = e => {
    input.select();
    document.execCommand('copy');
    e.target.focus();
    setCopied(true);
  };

  const emailChanged = value => {
    setEmail(value);
    setEmailValid(validator.isEmail(email));
  };

  const shareEmail = async () => {
    try {
      if (isLoading || !isEmailValid) {
        return;
      }
      setLoading(true);
      await sharePostViaEmail({ postId, email });
      setSent(true);
    } finally {
      setLoading(false);
    }
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Share Post</span>
        {copied && (
          <span>
            <Icon color="green" name="copy" />
            Copied
          </span>
        )}
        {sent && (
          <span>
            <Icon color="green" name="at" />
            Sent
          </span>
        )}
      </Modal.Header>
      <Modal.Content>
        <Input
          fluid
          action={{
            color: 'teal',
            labelPosition: 'right',
            icon: 'copy',
            content: 'Copy',
            onClick: copyToClipboard
          }}
          value={`${window.location.origin}/share/${postId}`}
          ref={ref => { input = ref; }}
        />
        <br />
        <br />
        <Input
          action={{
            color: 'teal',
            labelPosition: 'right',
            icon: 'at',
            content: 'Send to email',
            onClick: shareEmail,
            loading: isLoading
          }}
          placeholder="you@example.com"
          type="email"
          error={!isEmailValid}
          onChange={ev => emailChanged(ev.target.value)}
          onBlur={() => setEmailValid(validator.isEmail(email))}
        />
      </Modal.Content>
    </Modal>
  );
};

SharedPostLink.propTypes = {
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired
};

export default SharedPostLink;
