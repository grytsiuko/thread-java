import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Label, Icon, Form, TextArea, Button } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';
import ReactionsList from '../ReactionsList';
import { getComment } from '../../services/commentService';

const Comment = ({ comment, postId, likeComment, dislikeComment, deleteComment, userId, updateComment }) => {
  const {
    id,
    body,
    user,
    likeCount,
    dislikeCount,
    createdAt
  } = comment;

  const [isEditing, setEditing] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [editingBody, setEditingBody] = useState(body);
  const [isBodyValid, setBodyValid] = useState(true);

  const changeBody = value => {
    setEditingBody(value === '' ? null : value);
    setBodyValid(value !== '');
  };

  const handleUpdateComment = () => {
    if (isLoading || !isBodyValid) {
      return;
    }
    setLoading(true);
    updateComment({ body: editingBody, commentId: id, postId });
    setLoading(false);
    setEditing(false);
  };

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        {comment.user.id === userId && (
          <Label basic size="small" as="a" className={styles.btnRight} onClick={() => deleteComment(id, postId)}>
            <Icon name="trash" />
          </Label>
        )}
        {comment.user.id === userId && !isEditing && (
          <Label basic size="small" as="a" className={styles.btnRight} onClick={() => setEditing(true)}>
            <Icon name="edit" />
          </Label>
        )}
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(createdAt).fromNow()}
        </CommentUI.Metadata>
        <CommentUI.Text>
          <div style={{ fontSize: '0.85em', fontStyle: 'italic', marginBottom: 5 }}>
            {user.status}
          </div>
          <div>
            {isEditing
              ? (
                <Form onSubmit={handleUpdateComment}>
                  <TextArea placeholder="Comment" defaultValue={body} onChange={ev => changeBody(ev.target.value)} />
                  <Button type="submit" loading={isLoading} primary>Save</Button>
                  <Button onClick={() => setEditing(false)} secondary>Cancel</Button>
                </Form>
              )
              : (body)}
          </div>
        </CommentUI.Text>
        <CommentUI.Actions>
          <ReactionsList
            getWrapper={getComment}
            wrapperId={id}
            getReactions={r => r.reactions.filter(rr => rr.isLike)}
            trigger={(
              <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likeComment(id)}>
                <Icon name="thumbs up" />
                {likeCount}
              </Label>
            )}
          />
          <ReactionsList
            getWrapper={getComment}
            wrapperId={id}
            getReactions={r => r.reactions.filter(rr => !rr.isLike)}
            trigger={(
              <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikeComment(id)}>
                <Icon name="thumbs down" />
                {dislikeCount}
              </Label>
            )}
          />
        </CommentUI.Actions>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  postId: PropTypes.objectOf(PropTypes.any).isRequired,
  userId: PropTypes.objectOf(PropTypes.any).isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired
};

export default Comment;
