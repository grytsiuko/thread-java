import React, { useState } from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import { Form, Button, Message, Header, Input } from 'semantic-ui-react';

const RequestForm = ({ request }) => {
  const [email, setEmail] = useState('');
  const [isEmailValid, setEmailValid] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [isSent, setIsSent] = useState(false);
  const [isError, setIsError] = useState(false);

  const emailChanged = value => {
    setEmail(value === '' ? null : value);
    setEmailValid(validator.isEmail(value));
  };

  const sendRequest = () => {
    setIsSent(false);
    setIsError(false);
    if (isLoading || !isEmailValid) {
      return;
    }
    setLoading(true);
    request({ email })
      .then(() => setIsSent(true))
      .catch(() => setIsError(true))
      .finally(() => setLoading(false));
  };

  return (
    <Form onSubmit={sendRequest}>
      <br />
      {isSent && (
        <Message positive style={{ maxWidth: 400, margin: '0 auto' }}>
          <Message.Header>Sent</Message.Header>
        </Message>
      )}
      {isError && (
        <Message negative style={{ maxWidth: 400, margin: '0 auto' }}>
          <Message.Header>No user with such email</Message.Header>
        </Message>
      )}
      <Header as="h3" color="teal" textAlign="center">
        Send a link to reset password
      </Header>
      <Input
        icon="at"
        iconPosition="left"
        placeholder="Email"
        type="email"
        error={!isEmailValid}
        onChange={ev => emailChanged(ev.target.value)}
        onBlur={() => setEmailValid(Boolean(email))}
      />
      <br />
      <br />
      <Button type="submit" loading={isLoading} primary>
        Send
      </Button>
    </Form>
  );
};

RequestForm.propTypes = {
  request: PropTypes.func.isRequired
};

export default RequestForm;
