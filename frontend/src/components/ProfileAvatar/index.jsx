import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { getUserImgLink } from 'src/helpers/imageHelper';
import {
  Image,
  Button,
  Icon
} from 'semantic-ui-react';

import { uploadImage } from '../../services/imageService';
import styles from './styles.module.scss';

const ProfileAvatar = ({ user, updateCurrentAvatar: updateAvatar }) => {
  const [image, setImage] = useState(undefined);
  const [isUploading, setIsUploading] = useState(false);

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const uploadedImg = await uploadImage(target.files[0]);
      setImage(uploadedImg);
      updateAvatar({ imageId: uploadedImg.id, imageLink: uploadedImg.link });
    } finally {
      setIsUploading(false);
    }
  };

  return (
    <fragment>
      <Image centered src={getUserImgLink(user.image)} size="medium" circular />
      <br />
      {image?.imageLink && (
        <div className={styles.imageWrapper}>
          <Image className={styles.image} src={image?.imageLink} alt="post" />
        </div>
      )}
      <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
        <Icon name="image" />
        Change avatar
        <input name="image" type="file" onChange={handleUploadFile} hidden />
      </Button>
    </fragment>
  );
};

ProfileAvatar.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  updateCurrentAvatar: PropTypes.func.isRequired
};

export default ProfileAvatar;
