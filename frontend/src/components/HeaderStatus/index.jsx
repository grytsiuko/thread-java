import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Icon, Button, Input, Form } from 'semantic-ui-react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { updateCurrentUser } from '../../containers/Profile/actions';

const HeaderStatus = ({ user, updateCurrentUser: updateUser }) => {
  const maxStatusLength = 40;
  const status = (user.status && user.status.length > maxStatusLength)
    ? `${user.status.substring(0, maxStatusLength - 3)}...`
    : user.status;

  const [isEditing, setEditing] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [editingStatus, setEditingStatus] = useState(user.status);

  const statusEditingChanged = value => {
    setEditingStatus(value === '' ? null : value);
  };

  const updateForm = () => {
    if (isLoading) {
      return;
    }
    setLoading(true);
    updateUser({ status: editingStatus, username: user.username, email: user.email });
    setLoading(false);
    setEditing(false);
  };

  return (
    <fragment>
      {isEditing
        ? (
          <fragment>
            <Form onSubmit={updateForm}>
              <Input
                placeholder="Status"
                type="text"
                defaultValue={user.status}
                onChange={ev => statusEditingChanged(ev.target.value)}
              />
              <Button type="submit" loading={isLoading} primary>
                Save
              </Button>
              <Button onClick={() => setEditing(false)} secondary>
                Cancel
              </Button>
            </Form>
          </fragment>
        ) : (
          <fragment>
            {status}
            <Icon
              name="edit"
              onClick={() => setEditing(true)}
              style={{ cursor: 'pointer', marginLeft: 10 }}
            />
          </fragment>
        )}
    </fragment>
  );
};

const actions = { updateCurrentUser };

const mapStateToProps = () => ({
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

HeaderStatus.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  updateCurrentUser: PropTypes.func.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HeaderStatus);
