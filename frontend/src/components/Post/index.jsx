import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Button, Form, TextArea } from 'semantic-ui-react';
import moment from 'moment';

import { getPost } from 'src/services/postService';
import ReactionsList from '../ReactionsList';
import styles from './styles.module.scss';

const Post = ({ post, likePost, dislikePost, updatePost, deletePost, toggleExpandedPost, sharePost, userId }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;

  const [isEditing, setEditing] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [editingBody, setEditingBody] = useState(body);
  const [isBodyValid, setBodyValid] = useState(true);

  const changeBody = value => {
    setEditingBody(value === '' ? null : value);
    setBodyValid(value !== '');
  };

  const handleUpdatePost = () => {
    if (isLoading || !isBodyValid) {
      return;
    }
    setLoading(true);
    updatePost({ body: editingBody, postId: id });
    setLoading(false);
    setEditing(false);
  };

  const date = moment(createdAt).fromNow();
  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          {post.user.id === userId && (
            <Label basic size="small" as="a" className={styles.toolbarBtnRight} onClick={() => deletePost(id)}>
              <Icon name="trash" />
            </Label>
          )}
          {post.user.id === userId && !isEditing && (
            <Label basic size="small" as="a" className={styles.toolbarBtnRight} onClick={() => setEditing(true)}>
              <Icon name="edit" />
            </Label>
          )}
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>
          {isEditing
            ? (
              <Form onSubmit={handleUpdatePost}>
                <TextArea placeholder="Post Body" defaultValue={body} onChange={ev => changeBody(ev.target.value)} />
                <Button type="submit" loading={isLoading} primary>Save</Button>
                <Button onClick={() => setEditing(false)} secondary>Cancel</Button>
              </Form>
            )
            : (body)}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <ReactionsList
          getWrapper={getPost}
          wrapperId={id}
          getReactions={r => r.reactions.filter(rr => rr.isLike)}
          trigger={(
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
              <Icon name="thumbs up" />
              {likeCount}
            </Label>
          )}
        />
        <ReactionsList
          getWrapper={getPost}
          wrapperId={id}
          getReactions={r => r.reactions.filter(rr => !rr.isLike)}
          trigger={(
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
              <Icon name="thumbs down" />
              {dislikeCount}
            </Label>
          )}
        />
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  userId: PropTypes.objectOf(PropTypes.any).isRequired,
  deletePost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired
};

export default Post;
