import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { Header as HeaderUI, Image, Grid, Icon, Button } from 'semantic-ui-react';
import HeaderStatus from '../HeaderStatus';

import styles from './styles.module.scss';

const Header = ({ user, logout }) => (
  <div className={styles.headerWrp}>
    <Grid centered container columns="2">
      <Grid.Column>
        {user && (
          <fragment>
            <NavLink exact to="/">
              <Image circular size="mini" src={getUserImgLink(user.image)} style={{ float: 'left' }} />
            </NavLink>
            <fragment style={{ float: 'left', margin: 0, marginLeft: 10 }}>
              <NavLink exact to="/">
                <HeaderUI style={{ margin: 0 }}>
                  {user.username}
                </HeaderUI>
              </NavLink>
              <HeaderStatus user={user} />
            </fragment>
          </fragment>
        )}
      </Grid.Column>
      <Grid.Column textAlign="right">
        <NavLink exact activeClassName="active" to="/profile" className={styles.menuBtn}>
          <Icon name="user circle" size="large" />
        </NavLink>
        <Button basic icon type="button" className={`${styles.menuBtn} ${styles.logoutBtn}`} onClick={logout}>
          <Icon name="log out" size="large" />
        </Button>
      </Grid.Column>
    </Grid>
  </div>
);

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

export default connect(
  mapStateToProps
)(Header);
