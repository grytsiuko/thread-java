import * as authService from 'src/services/authService';
import { SET_USER } from './actionTypes';
import { setPostsAction } from '../Thread/actions';

const setToken = token => localStorage.setItem('token', token);

const setUser = user => async dispatch => dispatch({
  type: SET_USER,
  user
});

const setAuthData = (user = null, token = '') => (dispatch, getRootState) => {
  setToken(token); // token should be set first before user
  setUser(user)(dispatch, getRootState);
};

const handleAuthResponse = authResponsePromise => async (dispatch, getRootState) => {
  const { user, token } = await authResponsePromise;
  setAuthData(user, token)(dispatch, getRootState);
};

export const login = request => handleAuthResponse(authService.login(request));

export const register = request => handleAuthResponse(authService.registration(request));

export const logout = () => setAuthData();

export const loadCurrentUser = () => async (dispatch, getRootState) => {
  const user = await authService.getCurrentUser();
  setUser(user)(dispatch, getRootState);
};

export const resetPasswordAction = request => async () => {
  await authService.resetPassword(request);
};

export const requestPasswordAction = request => async () => {
  await authService.requestPassword(request);
};

export const updateCurrentUser = request => async (dispatch, getRootState) => {
  await authService.putCurrentUser(request);

  const mapUser = user => ({
    ...user,
    status: request.status,
    username: request.username,
    email: request.email
  });

  const mapPost = post => ({
    ...post,
    user: {
      ...post.user,
      username: request.username
    }
  });

  const { profile: { user }, posts: { posts } } = getRootState();
  if (posts) {
    const updatedPosts = (posts || []).map(p => ((p.user.id === request.userId) ? mapPost(p) : p));
    dispatch(setPostsAction(updatedPosts));
  }
  setUser(mapUser(user))(dispatch, getRootState);
};

export const updateCurrentAvatar = request => async (dispatch, getRootState) => {
  await authService.putCurrentAvatar(request);

  const mapUser = user => ({
    ...user,
    image: { id: request.imageId, link: request.imageLink }
  });

  const { profile: { user } } = getRootState();
  setUser(mapUser(user))(dispatch, getRootState);
};
