import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Grid,
  Input,
  Button,
  Form,
  Message
} from 'semantic-ui-react';
import validator from 'validator';

import { updateCurrentAvatar, updateCurrentUser } from './actions';
import ProfileAvatar from '../../components/ProfileAvatar';

const Profile = ({ user, updateCurrentUser: updateUser, updateCurrentAvatar: updateAvatar }) => {
  const [status, setStatus] = useState(user.status);
  const [username, setUsername] = useState(user.username);
  const [email, setEmail] = useState(user.email);
  const [isEmailValid, setEmailValid] = useState(true);
  const [isUsernameValid, setUsernameValid] = useState(true);
  const [isLoading, setLoading] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);
  const [isError, setIsError] = useState(false);

  const statusChanged = value => {
    setStatus(value === '' ? null : value);
  };

  const usernameChanged = value => {
    setUsername(value === '' ? null : value);
    setUsernameValid(value !== '');
  };

  const emailChanged = value => {
    setEmail(value === '' ? null : value);
    setEmailValid(validator.isEmail(value));
  };

  const updateForm = () => {
    setIsSuccess(false);
    setIsError(false);
    if (isLoading || !isUsernameValid || !isEmailValid) {
      return;
    }

    setLoading(true);
    updateUser({ status, username, email, userId: user.id })
      .then(() => setIsSuccess(true))
      .catch(() => setIsError(true))
      .finally(() => setLoading(false));
  };

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <ProfileAvatar user={user} updateCurrentAvatar={updateAvatar} />
        <br />
        <br />
        {isSuccess && (
          <Message positive style={{ maxWidth: 400, margin: '0 auto' }}>
            <Message.Header>Success</Message.Header>
          </Message>
        )}
        {isError && (
          <Message negative style={{ maxWidth: 400, margin: '0 auto' }}>
            <Message.Header>User with such username or email already exists</Message.Header>
          </Message>
        )}
        <br />
        <Form onSubmit={e => {
          e.preventDefault();
          updateForm();
        }}
        >
          <Input
            icon="user"
            iconPosition="left"
            placeholder="Username"
            type="text"
            defaultValue={user.username}
            error={!isUsernameValid}
            onChange={ev => usernameChanged(ev.target.value)}
            onBlur={() => setUsernameValid(Boolean(username))}
          />
          <br />
          <br />
          <Input
            icon="at"
            iconPosition="left"
            placeholder="Email"
            type="email"
            defaultValue={user.email}
            error={!isEmailValid}
            onChange={ev => emailChanged(ev.target.value)}
            onBlur={() => setEmailValid(Boolean(email))}
          />
          <br />
          <br />
          <Input
            icon="thumbtack"
            iconPosition="left"
            placeholder="Status"
            type="text"
            defaultValue={user.status}
            onChange={ev => statusChanged(ev.target.value)}
          />
          <br />
          <br />
          <Button type="submit" loading={isLoading} primary>
            Save
          </Button>
        </Form>
      </Grid.Column>
    </Grid>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  updateCurrentUser: PropTypes.func.isRequired,
  updateCurrentAvatar: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: {}
};

const actions = { updateCurrentUser, updateCurrentAvatar };

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
