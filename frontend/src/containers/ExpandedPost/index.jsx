import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import {
  likePost, dislikePost, toggleExpandedPost, addComment, updateComment,
  likeComment, dislikeComment, deleteComment, updatePost, deletePost
} from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';

const ExpandedPost = ({
  post,
  userId,
  sharePost,
  likePost: like,
  dislikePost: dislike,
  updatePost: update,
  deletePost: remove,
  toggleExpandedPost: toggle,
  likeComment: likeComm,
  dislikeComment: dislikeComm,
  deleteComment: removeComm,
  updateComment: updateComm,
  addComment: add
}) => (
  <Modal centered={false} open onClose={() => toggle()} style={{ zIndex: 1 }}>
    {post
      ? (
        <Modal.Content>
          <Post
            post={post}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggle}
            sharePost={sharePost}
            updatePost={update}
            deletePost={remove}
            userId={userId}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <Header as="h3" dividing>
              Comments
            </Header>
            {post.comments && post.comments
              .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
              .map(comment => (
                <Comment
                  key={comment.id}
                  comment={comment}
                  postId={post.id}
                  userId={userId}
                  likeComment={likeComm}
                  dislikeComment={dislikeComm}
                  deleteComment={removeComm}
                  updateComment={updateComm}
                />
              ))}
            <AddComment postId={post.id} addComment={add} />
          </CommentUI.Group>
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  userId: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost
});

const actions = {
  likePost,
  dislikePost,
  toggleExpandedPost,
  addComment,
  likeComment,
  dislikeComment,
  deleteComment,
  updateComment,
  updatePost,
  deletePost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
