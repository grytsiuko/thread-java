import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { resetPasswordAction } from 'src/containers/Profile/actions';
import Logo from 'src/components/Logo';
import { Grid, Header, Message } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import PasswordForm from 'src/components/PasswordForm';

const PasswordPage = ({ match, resetPasswordAction: reset }) => {
  const userId = match.params.userHash;
  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Logo />
        <Header as="h2" color="teal" textAlign="center">
          Reset your password
        </Header>
        <PasswordForm resetPassword={reset} userId={userId} />
        <Message>
          <NavLink exact to="/login">Go Back</NavLink>
        </Message>
      </Grid.Column>
    </Grid>
  );
};

PasswordPage.propTypes = {
  match: PropTypes.objectOf(PropTypes.any).isRequired,
  resetPasswordAction: PropTypes.func.isRequired
};

const actions = { resetPasswordAction };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(PasswordPage);
