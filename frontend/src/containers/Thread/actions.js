import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  DELETE_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST
} from './actionTypes';

export const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const deletePostAction = postId => ({
  type: DELETE_POST,
  postId
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const updatePost = post => async (dispatch, getRootState) => {
  await postService.updatePost(post);

  const mapPost = p => ({
    ...p,
    body: post.body
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(p => (p.id !== post.postId ? p : mapPost(p)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === post.postId) {
    dispatch(setExpandedPostAction(mapPost(expandedPost)));
  }
};

export const deletePost = postId => async dispatch => {
  await postService.deletePost(postId)
    .then(() => {
      dispatch(deletePostAction(postId));
      dispatch(setExpandedPostAction(null));
    })
    .catch(() => console.log('Trying to delete post of another user'));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

const refreshLikingPost = (postId, likeDiff, dislikeDiff, dispatch, getRootState) => {
  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + likeDiff,
    dislikeCount: Number(post.dislikeCount) + dislikeDiff
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

const refreshLikingComment = (commentId, likeDiff, dislikeDiff, dispatch, getRootState) => {
  const mapLikes = expandedPost => ({
    ...expandedPost,
    comments: expandedPost.comments.map(
      comment => (comment.id !== commentId
        ? comment
        : ({
          ...comment,
          likeCount: Number(comment.likeCount) + likeDiff,
          dislikeCount: Number(comment.dislikeCount) + dislikeDiff
        })
      )
    )
  });

  const { posts: { expandedPost } } = getRootState();

  dispatch(setExpandedPostAction(mapLikes(expandedPost)));
};

export const likePost = postId => async (dispatch, getRootState) => {
  const result = await postService.likePost(postId);
  const likeDiff = result?.id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const dislikeDiff = result?.isChanged ? -1 : 0; // if post reaction was disliked before then subtract it

  refreshLikingPost(postId, likeDiff, dislikeDiff, dispatch, getRootState);
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const result = await postService.dislikePost(postId);
  const dislikeDiff = result?.id ? 1 : -1; // if ID exists then the post was disliked, otherwise - dislike was removed
  const likeDiff = result?.isChanged ? -1 : 0; // if post reaction was liked before then subtract it

  refreshLikingPost(postId, likeDiff, dislikeDiff, dispatch, getRootState);
};

export const likeComment = commentId => async (dispatch, getRootState) => {
  const result = await commentService.likeComment(commentId);
  const likeDiff = result?.id ? 1 : -1; // if ID exists then the comment was liked, otherwise - like was removed
  const dislikeDiff = result?.isChanged ? -1 : 0; // if comment reaction was disliked before then subtract it

  refreshLikingComment(commentId, likeDiff, dislikeDiff, dispatch, getRootState);
};

export const dislikeComment = commentId => async (dispatch, getRootState) => {
  const result = await commentService.dislikeComment(commentId);
  const dislikeDiff = result?.id ? 1 : -1; // if ID exists - the comment was disliked, otherwise - dislike was removed
  const likeDiff = result?.isChanged ? -1 : 0; // if comment reaction was liked before then subtract it

  refreshLikingComment(commentId, likeDiff, dislikeDiff, dispatch, getRootState);
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const updateComment = comment => async (dispatch, getRootState) => {
  await commentService.updateComment(comment);

  const mapComment = c => ({
    ...c,
    body: comment.body
  });

  const mapExpandedPost = p => ({
    ...p,
    comments: (p.comments || []).map(c => ((c.id !== comment.commentId) ? c : mapComment(c)))
  });

  const { posts: { expandedPost } } = getRootState();

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapExpandedPost(expandedPost)));
  }
};

export const deleteComment = (commentId, postId) => async (dispatch, getRootState) => {
  await commentService.deleteComment(commentId)
    .then(() => {
      const mapComments = post => ({
        ...post,
        commentCount: Number(post.commentCount) - 1,
        comments: (post.comments || []).filter(c => c.id !== commentId)
      });

      const { posts: { posts, expandedPost } } = getRootState();
      const updated = posts.map(post => (post.id !== postId
        ? post
        : mapComments(post)));

      dispatch(setPostsAction(updated));

      if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));
      }
    }).catch(() => console.log('Trying to delete comment of another user'));
};
