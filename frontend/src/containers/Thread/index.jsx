/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
  loadPosts, loadMorePosts, likePost, dislikePost, toggleExpandedPost, addPost, deletePost, updatePost
} from './actions';

import styles from './styles.module.scss';

const postsFilter = {
  showUserId: undefined,
  hideUserId: undefined,
  likedByUserId: undefined,
  from: 0,
  count: 10
};

const Thread = ({
  userId,
  loadPosts: load,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  hasMorePosts,
  addPost: createPost,
  deletePost: removePost,
  likePost: like,
  updatePost: update,
  dislikePost: dislike,
  toggleExpandedPost: toggle
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [hideOwnPosts, setHideOwnPosts] = useState(false);
  const [showLikedPosts, setShowLikedPosts] = useState(false);

  const refreshThread = () => {
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    setHideOwnPosts(false);
    postsFilter.showUserId = showOwnPosts ? undefined : userId;
    postsFilter.hideUserId = undefined;
    refreshThread();
  };

  const toggleHideOwnPosts = () => {
    setHideOwnPosts(!hideOwnPosts);
    setShowOwnPosts(false);
    postsFilter.hideUserId = hideOwnPosts ? undefined : userId;
    postsFilter.showUserId = undefined;
    refreshThread();
  };

  const toggleShowLikedPosts = () => {
    setShowLikedPosts(!showLikedPosts);
    postsFilter.likedByUserId = showLikedPosts ? undefined : userId;
    refreshThread();
  };

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
        />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Hide my posts"
          checked={hideOwnPosts}
          onChange={toggleHideOwnPosts}
        />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only liked posts"
          checked={showLikedPosts}
          onChange={toggleShowLikedPosts}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          <Post
            post={post}
            likePost={like}
            deletePost={removePost}
            dislikePost={dislike}
            updatePost={update}
            toggleExpandedPost={toggle}
            sharePost={sharePost}
            key={post.id}
            userId={userId}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost sharePost={sharePost} userId={userId} />}
      {sharedPostId && <SharedPostLink postId={sharedPostId} close={() => setSharedPostId(undefined)} />}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  userId: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  updatePost,
  toggleExpandedPost,
  deletePost,
  addPost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);
